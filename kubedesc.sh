#!/bin/bash 
COMMAND='describe'
context=$1

resolv_context=1

KUBECTL=`which kubectl` 
FZF=`which fzf` 
match="[[:alnum:]][[:alnum:]]*"

log_error(){
  echo "$1"
  exit 2
}

if [[ -z "$1" && resolv_context -eq 1 ]];then
 context=`$KUBECTL config current-context`
fi 
context=${context/${match}/"--context $context"}

namespaces="preprod\nproduction\nsolar"
#namespaces=`$KUBECTL get namespaces ${context} -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'`
if [ -z "${namespaces}" ]; then
  log_error 'No namespaces found.'
fi
namespace=`echo -e "${namespaces}" | $FZF -1 --header="${context:-[current context]}"`
namespace=${namespace/${match}/"--namespace ${namespace}"}
pods="api-aaasasasasas\napi-zxnxnnxznxznxnxznnxzxnz"
#pods=`$KUBECTL get pods ${context} ${namespace} -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'`
if [ -z "${pods}" ]; then
  log_error 'No pods found.'
fi
pod_list=`echo -e "$pods" | $FZF -1 -m --header="${context:-[current context]} ${namespace}"` 
for pod in $pod_list; do
  echo "$KUBECTL $COMMAND $pod ${context} ${namespace}"
done

